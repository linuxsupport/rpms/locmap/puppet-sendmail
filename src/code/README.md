## puppet-sendmail  
This sendmail module is designed to install and configure the sendmail service.

### Parameters  
* `smart_host_enable` boolean to enable/disable smart host, default is true  
* `smart_host` define a smart host, default is cern.ch
* `accept_external_mail` boolean, recieve or not external mails via SMTP, default is true  
* `user_domain` define a user domain, default is mail.cern.ch
* `local_relay_enable` boolean, accept local mail relays, default is true  
* `afs_forward_support` boolean, enable/disable to afs automatic mail forwarding, default is true  
* `masquerade_enable` boolean, enable/disable masquerade feature, default is true  
* `root_email` define what should be the root e-mail , by default there is no value 
* `responsible_email` define the responsible user's e-mail, default is the e-mail from landb value  
* `local-users` a list of local users for mail redirections to local mailboxes, default is the root and any afs user existing in the system  

Puppet databindings allows all the above settings to be set via hiera, users can override the default values inside '/etc/puppet/userdata/module_names/sendmail/sendmail.yaml'  


```YAML
---
accept_external_mail: false
local-users:
- username1
- username2
```  

## Contact
Aris Boutselis <aris.boutselis@cern.ch>  

## Support
https://gitlab.cern.ch/linuxsupport/puppet-sendmail

