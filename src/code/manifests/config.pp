class sendmail::config {

  file { 'sendmail.mc':
    ensure  => present,
    path    => $sendmail::params::sendmailmc_file,
    owner   => $sendmail::params::owner,
    group   => $sendmail::params::file_group,
    mode    => $sendmail::params::mode,
    content => template($sendmail::params::sendmail_template),
    require => Class['sendmail::install'],
    notify  => Exec['sendmail_m4compile'],
  }

  file { 'genericstable':
    ensure  => file,
    path    => '/etc/mail/genericstable',
    owner   => root,
    group   => root,
    mode    => '0644',
    content => template('sendmail/genericstable.erb'),
    require => File['sendmail.cf'],
    notify  => Class['sendmail::service'],
  }

  exec { 'sendmail_m4compile':
    command     => 'm4 sendmail.mc > sendmail.cf',
    cwd         => '/etc/mail',
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    refreshonly => true,
    require     => File['sendmail.mc'],
  }

  file { 'sendmail.cf':
    ensure  => present,
    path    => $sendmail::params::sendmailcf_file,
    owner   => $sendmail::params::owner,
    group   => $sendmail::params::file_group,
    mode    => $sendmail::params::mode,
    require => Exec['sendmail_m4compile'],
    notify  => Class['sendmail::service'],
  }

  if $sendmail::params::local_relay_enable {
    file { 'local-users':
      ensure  => present,
      path    => $sendmail::params::local_users_file,
      owner   => $sendmail::params::owner,
      group   => $sendmail::params::file_group,
      mode    => $sendmail::params::mode,
      content => template($sendmail::params::local_users_template),
      require => File['sendmail.cf'],
      notify  => Class['sendmail::service'],
    }
  }

  file { 'forward':
    ensure  => present,
    path    => $sendmail::params::forward_file,
    owner   => $sendmail::params::owner,
    group   => $sendmail::params::file_group,
    mode    => $sendmail::params::mode,
    content => template($sendmail::params::forward_template),
    require => File['sendmail.cf'],
    notify  => Class['sendmail::service'],
}

#root email forward.
  mailalias { 'root':
    ensure    => $sendmail::params::root_email? { ''         => absent,
    default => present }, #no forward if root_email is empty
    recipient => $sendmail::params::root_email,
    require   => Class['sendmail::install'],
    notify    => Class['sendmail::newaliases']
  }

#patch for dyndns

  exec { 'insert dyndns.cern.ch to local-host-names':
    command     => 'echo `/bin/hostname -s`.dyndns.cern.ch >> /etc/mail/local-host-names',
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    require     => Class['sendmail::install'],
    unless      => 'grep `/bin/hostname -s`.dyndns.cern.ch /etc/mail/local-host-names  2>/dev/null',
  }

  ->

  exec { 'update sendmail configuration':
    command     => 'make',
    cwd         => '/etc/mail',
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    refreshonly => true,
    notify      => Class['sendmail::service'],
  }

}
