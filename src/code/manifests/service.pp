class sendmail::service {

  service {
    'sendmail':
      ensure     => running,
      enable     => true,
      hasrestart => true,
      hasstatus  => true;
    # In case postfix is running on the system,
    # it must be stopped before starting sendmail
    'postfix':
      ensure     => stopped,
      enable     => false,
      hasrestart => true,
      hasstatus  => true,
      before     => Service['sendmail'],
  }

}

