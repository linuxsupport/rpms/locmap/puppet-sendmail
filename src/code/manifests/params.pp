  class sendmail::params {
  $smart_host_enable    = lookup({"name" => "smart_host_enable", "default_value" => true})

  $smart_host           = lookup({"name" => "smart_host", "default_value" => 'cern.ch'})
  $accept_external_mail = lookup({"name" => "accept_external_mail", "default_value" => true})
  $user_domain          = lookup({"name" => "user_domain", "default_value" => 'cern.ch'})
  $local_relay_enable   = lookup({"name" => "local_relay_enable", "default_value" => true})
  $afs_forward_support  = lookup({"name" => "afs_forward_support", "default_value" => true})
  $masquerade_enable    = lookup({"name" => "masquerade_enable", "default_value" => true})
  $root_email           = lookup({"name" => "root_email", "default_value" => undef})
  $responsible_email    = lookup({"name" => "mails", "default_value" => []})
  $local_users          = lookup({"name" => "local_users", "default_value" => []})
  $sendmail_template    = 'sendmail/sendmail.mc.erb'
  $local_users_template = 'sendmail/local-users.erb'
  $forward_template     = 'sendmail/forward.erb'
  #Application related parameters
  $sendmailmc_file      = '/etc/mail/sendmail.mc'
  $sendmailcf_file      = '/etc/mail/sendmail.cf'
  $local_users_file     = '/etc/mail/local-users'
  $forward_file         = '/root/.forward'
  $file_group           = 'root'
  $owner                = 'root'
  $mode                 = '0644'
  $generics_domain      = $facts['networking']['fqdn']
  $root_from_format     = "root_${facts['networking']['hostname']}@${user_domain}"

}
