#Takes care of running newaliases when /etc/aliases is touched,
#i.e when we use the mailalias resource type.
#This is a workaround for http://projects.puppetlabs.com/issues/939 
#NB: the aliases.db may only get updated at the next Puppet run if this is executed before Puppet updates /etc/aliases
#hence it is useful to specify notify=>Class['sendmail::newliases'] in mailalias declarations.
class sendmail::newaliases {
  
  #Run newaliases to rebuild the aliases.db file whenever the aliases file has been touched
  #and is more recent than aliases.db
  exec { 'sendmail_newaliases':
    command   => '/usr/bin/newaliases',
    #the following returns 1 if /etc/aliases.db is more recent, 0 if /etc/aliases is more recent or aliases.db is absent (and newaliases needs to be run)
    onlyif    => "/bin/bash -c 'test /etc/aliases -nt /etc/aliases.db'",
    require   => Class['sendmail::config'], #config must have been done first (especially local-users)
    logoutput => 'on_failure'
  }

  
}