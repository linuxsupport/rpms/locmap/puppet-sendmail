Name:           puppet-sendmail
Version:        2.3
Release:        1%{?dist}
Summary:        Masterless puppet module for sendmail

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Masterless puppet module for sendmail.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/sendmail/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/sendmail/
touch %{buildroot}/%{_datadir}/puppet/modules/sendmail/linuxsupport

%files -n puppet-sendmail
%{_datadir}/puppet/modules/sendmail
%doc code/README.md

%changelog
* Mon Jan 29 2024 Ben Morrice <ben.morrice@cern.ch> - 2.3-1
- Default to a valid domain for sending mails from root (CRM-4745)

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.2-2
- fix requires on puppet-agent

* Fri Jan 15 2021 Ben Morrice <ben.morrice@cern.ch> - 2.2-1
- fix deprecation warnings with hiera lookups

* Wed Aug 07 2019 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- respect params for sendmail.mc.erb

* Wed May 02 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- New version for locmap 2.0

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
